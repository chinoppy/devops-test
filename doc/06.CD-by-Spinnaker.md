# Spinnaker を使った継続的デリバリ (CD) に対応したデプロイ


- Spinnakerのチュートリアルは以下を参考にして作っています
  - https://cloud.google.com/solutions/continuous-delivery-spinnaker-kubernetes-engine?hl=ja



## ID およびアクセス管理を構成する
- Cloud Identity Access Management（Cloud IAM）サービス アカウントを作成して Spinnaker に権限を委任し、Cloud Storage にデータを保存できるようにします。

- サービスアカウントを生成

```
$ gcloud iam service-accounts create spinnaker-account \
    --display-name spinnaker-account
```

- IAM のUIに spinnaker-account が生成されている
  - IAMと管理 > サービスアカウント
    - 「名前」が `spinnaker-account` になっているアカウントが追加されている

- 後のコマンドで使用するために、サービス アカウントのメールアドレスと現在のプロジェクト ID を環境変数に格納します。

```
$ export SA_EMAIL=$(gcloud iam service-accounts list \
    --filter="displayName:spinnaker-account" \
    --format='value(email)')
$ export PROJECT=$(gcloud info --format='value(config.project)')
$ echo $SA_EMAIL $PROJECT
```

- storage.admin 役割をサービス アカウントにバインドします。

```
$ gcloud projects add-iam-policy-binding \
    $PROJECT --role roles/storage.admin --member serviceAccount:$SA_EMAIL
```

- サービス アカウントキーをダウンロードします。後のステップで Spinnaker をインストールして、このキーを Kubernetes Engine にアップロードします。

```
$ gcloud iam service-accounts keys create spinnaker-sa.json --iam-account $SA_EMAIL
```


## Spinnaker パイプラインをトリガーする Cloud Pub/Sub を設定する

- Container Registry からの通知に使用する Cloud Pub/Sub トピックを作成します
  - データ量は、pull、push、パブリッシュの各オペレーションのメッセージ データと属性データを使用して計算されます。
  - 最初の 10 GB まで無料
  - URLの資料では、pubsubがbetaになっているが、betaはいらない

```
$ gcloud pubsub topics create projects/$PROJECT/topics/gcr
```

- GUI の Pub/Subに `gcr` のトピックが追加されていることを確認

- イメージの push についての通知を受け取れるように、Spinnaker から読み取ることができるサブスクリプションを作成します。

```
$ gcloud pubsub subscriptions create gcr-triggers \
    --topic projects/${PROJECT}/topics/gcr
```

- Spinnakerサービスアカウントにgcr-triggers subscriptionを使用するの許可を与える。
  - `gcloud pubsub beta subscriptions add-iam-policy-binding gcr-triggers` のコマンドは `beta` なので注意

```
$ export SA_EMAIL=$(gcloud iam service-accounts list \
    --filter="displayName:spinnaker-account" \
    --format='value(email)')
$ gcloud beta pubsub subscriptions add-iam-policy-binding gcr-triggers \
    --role roles/pubsub.subscriber --member serviceAccount:$SA_EMAIL
```

## Helm を使用した Spinnaker のデプロイ

### Helm をインストールする

- Helm バイナリをダウンロード

```
$ wget https://storage.googleapis.com/kubernetes-helm/helm-v2.10.0-linux-amd64.tar.gz
```

- ファイルをローカル システムに解凍

```
$ tar zxfv helm-v2.10.0-linux-amd64.tar.gz
$ cp linux-amd64/helm .
```

- クラスタ内のHelm のサーバー側である Tillerにcluster-adminロールを付与します。

```
$ kubectl create clusterrolebinding user-admin-binding --clusterrole=cluster-admin --user=$(gcloud config get-value account)
$ kubectl create serviceaccount tiller --namespace kube-system
$ kubectl create clusterrolebinding tiller-admin-binding --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
```

- Spinnaker に cluster-admin 役割を付与し、すべての名前空間にリソースをデプロイできるようにします。

```
$ kubectl create clusterrolebinding --clusterrole=cluster-admin --serviceaccount=default:default spinnaker-admin
```

- Helm を初期化して、クラスタに Tiller をインストールします

```
$ ./helm init --service-account=tiller
$ ./helm update
$ ./helm version   # 確認
```


### Spinnaker を構成する

- Spinnaker でパイプライン構成を保存するためのバケットを作成します。

```
$ export PROJECT=$(gcloud info \
    --format='value(config.project)')
$ export BUCKET=$PROJECT-spinnaker-config
$ gsutil mb -c regional -l us-central1 gs://$BUCKET
```

- Spinnaker のインストール方法の構成を記述するファイル（spinnaker-config.yaml）を作成します。

```
$ export SA_JSON=$(cat spinnaker-sa.json)
$ export PROJECT=$(gcloud info --format='value(config.project)')
$ export BUCKET=$PROJECT-spinnaker-config
$ cat > spinnaker-config.yaml <<EOF
gcs:
  enabled: true
  bucket: $BUCKET
  project: $PROJECT
  jsonKey: '$SA_JSON'

dockerRegistries:
- name: gcr
  address: https://gcr.io
  username: _json_key
  password: '$SA_JSON'
  email: 1234@5678.com

# Disable minio as the default storage backend
minio:
  enabled: false

# Configure Spinnaker to enable GCP services
halyard:
  spinnakerVersion: 1.10.2
  image:
    tag: 1.12.0
  additionalScripts:
    create: true
    data:
      enable_gcs_artifacts.sh: |-
        \$HAL_COMMAND config artifact gcs account add gcs-$PROJECT --json-path /opt/gcs/key.json
        \$HAL_COMMAND config artifact gcs enable
      enable_pubsub_triggers.sh: |-
        \$HAL_COMMAND config pubsub google enable
        \$HAL_COMMAND config pubsub google subscription add gcr-triggers \
          --subscription-name gcr-triggers \
          --json-path /opt/gcs/key.json \
          --project $PROJECT \
          --message-format GCR
EOF
```

### Spinnaker チャートをデプロイする

- Helm コマンドライン インターフェースを使用して、構成セットとともにチャートをデプロイします。
- このコマンドは完了するまでに 5〜10 分程度かかります。

```
$ ./helm install -n cd stable/spinnaker -f spinnaker-config.yaml \
    --timeout 9000 --version 1.1.6 --wait
```

- pod の起動を待つ
  - Pending じゃなくなるように

```
$ kubectl get pod
```

- コマンドが完了したら、次のコマンドを実行して、Cloud Shell から Spinnaker UI へのポート転送を設定します

```
$ export DECK_POD=$(kubectl get pods --namespace default -l "cluster=spin-deck" \
    -o jsonpath="{.items[0].metadata.name}")
$ kubectl port-forward --namespace default $DECK_POD 8080:9000 >> /dev/null &
```


- Spinnaker ユーザー インターフェースを開くには、Cloud Shell ウィンドウの最上部で [ウェブでプレビュー] をクリックし、[プレビューのポート: 8080] をクリックします。
  - ようこそ画面が表示されてから、Spinnaker UI が表示されます。


### Docker イメージのビルド

#### ソースコードリポジトリを作成し、Cloud Buildをセットアップ

- Cloud Source Repositories を有効にする
  - 最大 5 名のプロジェクト ユーザー1 が無料で使用できます
  - 無料枠には最大計 50 GB の無料ストレージと、1 か月あたり 50 GB の無料下りバイトが用意されています
  - ブラウザのUIから作成した
    - GitHubへ接続しようとしたが、うまくいかなかった
      - 「リポジトリに接続できませんでした」というエラー
      - GitHubの方がUXが良いから、そこが更新されたらミラーリングするといった形が良かったけど、、、
    - 何回かやり直したら接続できた（意味不明）
      - もう一回やってみたら、やっぱり失敗する
      - GitHubへの接続はやめる
  - `devops-test` というリポジトリを作成
- Cloud Shell にクローンしたコードに対して、ソースリポジトリを追加してプッシュする 

```
$ cd ~/devops-test
$ git config --global user.email "$(gcloud config get-value core/account)"
$ export USERNAME=$(whoami)
$ git config --global user.name $USERNAME
$ git config --list
$ git config credential.helper gcloud.sh
$ git remote add google https://source.developers.google.com/p/$PROJECT/r/devops-test
$ git add .
$ git commit -m "commit on cloud shell"
$ git push --all google
```

- Source Repositories にコード一式が追加される

- Cloud Build を設定する
  - Cloud Buildは、$0.003/ビルド分。1 日あたり最初の 120 ビルド分は無料です。
  - Container Registry にイメージがプッシュされるようにセット
    - Cloud Storage のストレージと下りネットワークに対してのみ課金（基本無料枠でいけるかと）
  - Cloud Source Repositories の設定に「このリポジトリのビルドトリガーの管理 Cloud Build トリガー」があるのでここから設定
  - 名前: devops-test-by-tags
  - トリガーのタイプ: タグ
  - タグ（正規表現）: v.*
  - ビルド設定: Cloud Build 構成ファイル（yaml または json)
  - Cloud Build 構成ファイルの場所: /cloudbuild.yaml


#### Spinnaker で使用するための Kubernetes デプロイを準備します

- バケット作成
  - Spinnaker は、クラスタにデプロイするために Kubernetes マニフェストにアクセスする必要があります
  - Cloud Build での CI プロセス中にマニフェストを保存する Cloud Storage バケットを作成します
  - マニフェストが Cloud Storage に保存されると、Spinnaker はパイプラインの実行中にマニフェストをダウンロードして適用できます

```
$ export PROJECT=$(gcloud info --format='value(config.project)')
$ gsutil mb -l us-central1 gs://$PROJECT-kubernetes-manifests
```

- バケットのバージョニングを有効化

```
$ gsutil versioning set on gs://$PROJECT-kubernetes-manifests
```

- Cloud Strage にバケットができているので画面から確認


- kubernetes の deployment に書かれている、利用イメージのバージョンの指定を消します

```
$ sed -i s/:v0.0.1//g k8s/deployment*
$ cat k8s/deployment.yaml
```

- コード修正

```
$ sed -i s/DevOps/Spinnaker/g web/app.js
$ sed -i s/greenyellow/red/g web/app.js
$ cat web/app.js
```


- コミットし、Tagを `v1.0.0` をセットしてプッシュする

```
$ git commit -a -m "Modify config and update code"
$ git tag v1.0.0
$ git push --tags google
```

- Cloud Build をGUIで開き、履歴の部分でビルドが成功していることを確認
- Container Registry に `v1.0.0` のイメージが登録されていることを確認


### デプロイメント パイプラインの構成

-  Kubernetes クラスタに Spinnaker を使ってデプロイします

- spin CLI をインストール
  - ダウンロードし、実行権限をつける(Cloud Shellで)

```
$ cd
$ curl -LO https://storage.googleapis.com/spinnaker-artifacts/spin/1.6.0/linux/amd64/spin
$ chmod +x spin
$ ./spin --version
```

- デプロイメント パイプラインを作成
  - `Could not read configuration file from xxxxxxx` という警告は無視でOK

- spinnaker に `devops-test` というアプリケーションの設定を作成

```
$ ./spin application save --application-name devops-test \
                        --owner-email "$(gcloud config get-value core/account)" \
                        --cloud-providers kubernetes \
                        --gate-endpoint http://localhost:8080/gate
```

- "v" という接頭辞が付いた Docker イメージが Container Registry に到着したことを検出するパイプラインを構成する
- チェックアウトしたコードの中にある `spinnaker/pipeline-deploy.json` のプロジェクトIDを変換する

```
$ cd ~/devops-test
$ export PROJECT=$(gcloud info --format='value(config.project)')
$ sed -i s/PROJECT/$PROJECT/g spinnaker/pipeline-deploy.json
$ cat spinnaker/pipeline-deploy.json
```

- パイプラインをSpinnakerに登録する

```
$ cd
$ ./spin pipeline save --gate-endpoint http://localhost:8080/gate -f devops-test/spinnaker/pipeline-deploy.json
```

### 手動でパイプラインを起動

- SpinnakerのGUIから
- 画面上部の Applications をクリック
- `devops-test` をクリック
- 上部にある `pipeline` をクリックします
- `Start Manual Execution` をクリック
- 進行状況バーが途中で止まるので、ブラウザでアクセスしてみる
  - 前の Istio の章でアクセスしたIPアドレスにブラウザでアクセス

```
$ kubectl get svc istio-ingressgateway -n istio-system
```

- カナリア環境は新しい画面（背景が赤でメッセージが変わっている）
- プロダクションは、前のまま

### カナリアで確認したアプリを production 環境へ反映

- 進行状況バー で Deploy to Production を Continue でクリック
- 完了後にブラウザでアクセス


### バージョンアップを実行してみる

```
$ cd ~/devops-test
$ sed -i s/red/blue/g web/app.js
$ cat web/app.js
```

- コミットし、Tagを `v1.0.1` をセットしてプッシュする

```
$ git commit -a -m "Version up with canary deploy"
$ git tag v1.0.1
$ git push --tags google
```


### カナリアデプロイの確認

- `v.*` のタグをプッシュすると、自動的にCode Build が起動し、 v1.0.1 のイメージが Container Registry 登録される
- その後、自動でSpinnaker が起動し、カナリア環境までのデプロイを完了させる
- ブラウザでアクセス
  - カナリア環境は新しい画面（背景が青に変わる）
  - プロダクションは、前のまま


### カナリアデプロイで確認したアプリを本番環境へ適用

- SpinnalerのGUIのパイプラインの進行状況バーで、 Deploy to Production を Continue へ
- ブラウザでアクセスすると、本番環境も更新できている



**おつかれさまでした。次のセクションに進みます。**

